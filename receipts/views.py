from django.shortcuts import render, redirect
from .models import Receipt
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm


# 8.1 Protect the list view for the Receipt model so that only a person
# who has logged in can access it.


@login_required
def show_receipt(request):
    # 8.2 Change the queryset of the view to filter the Receipt objects where
    # purchaser equals the logged in user. vvv
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list}
    return render(request, "receipts/list.html", context)


# Create a create view for the Receipt model that will allow someone to enter the vendor,
# total, tax, date, category, and account properties in the form and handle the form submission
# to create a new Receipt. (You may want to use a ModelForm.)
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)
