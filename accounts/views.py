from django.shortcuts import render, redirect
from .forms import LogInForm, SignUpForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# Write a function that will:
# display that form when it is a GET method
# try to log the person in when it is a POST method
def user_login(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LogInForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


# 9.1 Create a function in accounts/views.py that logs
# a person out then redirects them to the URL path registration named "login"


def user_logout(request):
    logout(request)
    return redirect("login")


# 10.2 Create a view function that will show the login form for an HTTP GET,
#  and try to create a new user for an HTTP POST.
def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            confirm = form.cleaned_data["password_confirmation"]
            if password == confirm:
                # 10.5 You'll need to use the special create_user method to create a new
                #  user account from their username and password.
                user = User.objects.create_user(username, password=password)
                # 10.6 You'll need to use the login function that logs an account in.
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
