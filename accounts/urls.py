from django.urls import path
from .views import user_login, user_logout, user_signup


urlpatterns = [
    # 7.4 Register the function in your accounts urls.py with the path "login/"
    #  and the name "login".
    path("login/", user_login, name="login"),
    # 9.2 In the accounts/urls.py, register that view in your urlpatterns
    # list with the path "logout/" and the name "logout".
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name="signup"),
]
