from django import forms

# Create a form that has two fields in the accounts Django app with:
# A username field with a maximum length of 150 characters
# A password field with:
# a maximum length of 150 characters
# the PasswordInput as its widget
class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())


# 10.1 Create a sign-up form with the following fields:
# username that has a max length of 150 characters
# password that has a max length of 150 characters and uses a PasswordInput
# password_confirmation that has a max length of 150 characters and uses a PasswordInput
class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput()
    )
